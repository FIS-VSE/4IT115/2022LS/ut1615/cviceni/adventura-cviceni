package cz.vse.xname.adventura1615.main;

import cz.vse.xname.adventura1615.logika.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HomeController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> panelVychodu;
    @FXML private TextField vstup;
    @FXML private TextArea vystup;
    @FXML private Button tlacitkoOdesli;

    private IHra hra = new Hra();
    private Map<String, Point2D> souradniceProstoru;
    private Map<String, ImageView> obrazkyProstoru;

    @FXML
    private void initialize() {
        hra.getHerniPlan().registruj(this);
        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
        aktualizujPanelVychodu();
        vytvorSouradniceProstoru();
        aktualizujPolohuHrace();
        vytvorObrazkyProstoru();
        panelVychodu.setCellFactory(listView -> new BunkaProstoru());
    }

    private void vytvorSouradniceProstoru() {
        souradniceProstoru = new HashMap<>();
        souradniceProstoru.put("domeček", new Point2D(14,75));
        souradniceProstoru.put("les", new Point2D(86,29));
        souradniceProstoru.put("hluboký_les", new Point2D(165, 74));
        souradniceProstoru.put("jeskyně", new Point2D(173, 169));
        souradniceProstoru.put("chaloupka", new Point2D(251, 29));
    }

    private void aktualizujPolohuHrace() {
        Prostor prostor = hra.getHerniPlan().getAktualniProstor();
        Point2D souradnice = souradniceProstoru.get(prostor.getNazev());
        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }

    private void aktualizujPanelVychodu() {
        panelVychodu.getItems().clear();
        Collection<Prostor> vychody = hra.getHerniPlan().getAktualniProstor().getVychody();
        panelVychodu.getItems().addAll(vychody);
    }

    @FXML
    private void odesliVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        vstup.clear();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        vystup.appendText("> "+prikaz+"\n\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            tlacitkoOdesli.setDisable(true);
            panelVychodu.setDisable(true);
        }
    }

    @Override
    public void aktualizuj(PredmetPozorovani predmetPozorovani) {
        aktualizujPanelVychodu();
        aktualizujPolohuHrace();
    }

    public void klikPanelVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = panelVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor == null) return;
        String prikaz = PrikazJdi.NAZEV+" "+novyProstor.getNazev();
        zpracujPrikaz(prikaz);
    }

    public void klikHrac(MouseEvent mouseEvent) {
        zpracujPrikaz(PrikazObsahBatohu.NAZEV);
    }

    public void zobrazNapovedu(ActionEvent actionEvent) {
        Stage okno = new Stage();
        WebView ww = new WebView();
        okno.setScene(new Scene(ww));
        ww.getEngine().load(getClass().getResource("napoveda.html").toExternalForm());
        okno.show();
    }

    class BunkaProstoru extends ListCell<Prostor> {
        @Override
        protected void updateItem(Prostor prostor, boolean empty) {
            super.updateItem(prostor, empty);
            if(!empty) {
                setText(prostor.getNazev());
                setGraphic(obrazkyProstoru.get(prostor.getNazev()));
            } else {
                setText(null);
                setGraphic(null);
            }
        }
    }

    private void vytvorObrazkyProstoru() {
        obrazkyProstoru = new HashMap<>();
        obrazkyProstoru.put("les", getObrazekProstoru("les.jpg"));
    }

    private ImageView getObrazekProstoru(String souborObrazku) {
        String cestaObrazky = getClass().getResource("/obrazkyProstoru").toExternalForm();
        ImageView obrazek = new ImageView(cestaObrazky+souborObrazku);
        obrazek.setFitHeight(80);
        obrazek.setPreserveRatio(true);
        return obrazek;
    }
}
