module cz.vse.xname.adventura1615 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    exports cz.vse.xname.adventura1615.main;
    opens cz.vse.xname.adventura1615.main to javafx.fxml;
}